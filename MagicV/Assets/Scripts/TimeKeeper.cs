﻿using UnityEngine;
using System.Collections;

public class TimeKeeper : MonoBehaviour {
	public float timer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer >= 2) {
			GetComponent<EnemyCreater>().CreateInterval=1f;
		}
		if (timer >= 5) {
			GetComponent<EnemyCreater>().ChangeEnemy=true;
		}

	}
}
