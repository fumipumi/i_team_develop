﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public Animation ani;
	public float rotMax; // 回転速度
	public Transform target;
	public float speed2=-0.1f;
	public float HitReactiontime=2f;
	public float HitReactiontimer;
	public AudioClip audioClip1;
	public AudioClip audioClip2;
	public AudioClip audioClip3;
	public AudioClip audioClip4;
	AudioSource audioSource;
	void Start()
	{
		target = GameObject.Find("player").transform;
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip3;
		audioSource.Play();

	}

	void Update()
	{
		HitReactiontimer += Time.deltaTime;
		if (HitReactiontimer >= 0) {
			//transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z-speed);
			ani = GetComponent<Animation> ();
			ani.Play ("Allosaurus_Run");
			transform.rotation = Quaternion.Slerp (Quaternion.LookRotation (-(target.position - transform.position)), transform.rotation, 0.5f);
			transform.Translate (0, 0, speed2); 
		}
	}
	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "Magic") {
			HitReactiontimer = -1 * HitReactiontime;
			ani = GetComponent<Animation> ();
			ani.Play ("Allosaurus_Die");
			audioSource.clip = audioClip1;
			audioSource.Play();
			audioSource.clip = audioClip4;
			audioSource.Play();
			GameObject.Find ("GameController").GetComponent<ScoreKeeper> ().Score += 100;
			Destroy (other.gameObject,2f);
			Destroy (gameObject, 2f);
		}
		
		if (other.gameObject.tag == "MainCamera") {
			if (HitReactiontimer >= 0) {
				HitReactiontimer = -1 * HitReactiontime;
				GameObject.Find ("GameController").GetComponent<ScoreKeeper> ().HitPoint -= 10;
				audioSource.clip = audioClip2;
				audioSource.Play();
				Debug.Log(other.gameObject.tag);
				ani = GetComponent<Animation> ();
				ani.Play ("Allosaurus_Attack02");
			}
		}
	}
}

