﻿using UnityEngine;
using System.Collections;

public class EnemyCreater : MonoBehaviour {
	public GameObject Enemy;
	public GameObject Enemy2;
	public GameObject MagicCircle;
	public GameObject Effects;
	public bool ChangeEnemy=false;
	public float CreateInterval=2f;

	private GameObject clone;
	private GameObject cloneCircle;
	private GameObject cloneEffects;
	// Use this for initialization
	IEnumerator Start() {
		while (true) {
			float x = Random.Range (408, 413);
			float y = 0f;
			float z = Random.Range (15, 30);

			if(!ChangeEnemy){
				clone = (GameObject) Instantiate (Enemy, new Vector3 (x, 163, 50), transform.rotation);
				clone.transform.parent = GameObject.Find ("Cam_All").transform;
			}
			if(ChangeEnemy){
				clone = (GameObject) Instantiate (Enemy2, new Vector3 (x, 163, 50), transform.rotation);
				clone.transform.parent = GameObject.Find ("Cam_All").transform;
			}
			cloneCircle = (GameObject) Instantiate (MagicCircle, new Vector3 (x, 163-0.5f, 50), transform.rotation);
			cloneCircle.transform.parent = GameObject.Find ("Cam_All").transform;
			cloneEffects = (GameObject) Instantiate (Effects, new Vector3 (x, 163, 50), transform.rotation);
			cloneEffects.transform.parent = GameObject.Find ("Cam_All").transform;
			yield return new WaitForSeconds (CreateInterval);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
