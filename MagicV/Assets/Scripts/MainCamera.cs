﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	public GameObject Magic;
	public GameObject Magic2;
	public float speed = 10;
	public float ShotInterval=0.5f;
	private float ShotIntervalTimer=0f;
	public bool Skill=false;
	public float SkillTimer=10f;
	private Vector3 position;
	private Vector3 screenToWorldPointPosition;
	// Update is called once per frame(毎フレーム呼ばれる)
	void Update () {
		ShotIntervalTimer += Time.deltaTime;
		if(Input.GetKey(KeyCode.Space)&&ShotIntervalTimer>=0){

			ShotIntervalTimer=-1*ShotInterval;
			if(SkillTimer>=0&&Skill){
				for (int i=0; i<transform.childCount; i++){
					Transform shotPosition = transform.GetChild(i);
					Shoot(shotPosition);
				}
			}
			Shoot();
		}
		if(Input.GetKey(KeyCode.UpArrow )|| Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)){
		    transform.position=new Vector3(transform.position.x+Input.GetAxis("Horizontal")*speed,0,transform.position.z);
		}
	}
	
	void Shoot () {
		GameObject obj = GameObject.Instantiate(Magic)as GameObject;
		if(ShotInterval==0.1f)obj = GameObject.Instantiate(Magic2)as GameObject;
		obj.transform.position = new Vector3(transform.position.x+1,transform.position.y,transform.position.z);
		//obj.transform.Rotate(new Vector3(300f,0f,0f));
		Vector3 force;
		force = this.gameObject.transform.forward * 3000;
		obj.GetComponent<Rigidbody>().AddForce (force);
	}
	void Shoot (Transform ShotPosition) {
		GameObject obj = GameObject.Instantiate(Magic)as GameObject;
		if(ShotInterval==0.1f)obj = GameObject.Instantiate(Magic2)as GameObject;
		obj.transform.position = new Vector3(transform.position.x+1,transform.position.y,transform.position.z);
		obj.transform.Rotate(new Vector3(300f,0f,0f));
		Vector3 force;
		force = this.gameObject.transform.forward * 3000;
		obj.GetComponent<Rigidbody>().AddForce (force);
	}


}