using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

	public float Duration;
	public GameObject mainCamera;
	public GameObject door;
	public string SceneName;
	public GameObject fireEffect;
	private bool startFlg = false;
	private bool doorOpenFlg = false;
	private float s = 0;

	// Use this for initialization
	void Start () {
	
	}

	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0 || Input.GetMouseButton (0)) {
			s = mainCamera.transform.position.z;

			startFlg = true;

			Invoke ("doorOpenStart", 1.5f);
			Invoke ("changeScene", 1.5f);
			Invoke ("fireStart", 3.0f);

		}

		if (startFlg) {
			s += 0.1f;
			mainCamera.transform.position = new Vector3( 0.0f , 1.0f , s );
		}

		if (doorOpenFlg) {
			doorOpen();
		}
	}

	void changeScene()
	{
		SceneManager.StartAlphaFade (Color.white, false, 3.0f, 0.0f,
		                             () => {
			Application.LoadLevel (SceneName);}
		);
	}

	void doorOpenStart() {
		doorOpenFlg = true;
	}

	void doorOpen() {
		door.transform.Rotate (0, 1, 0);
	}

//	void fireStart() {
//		Instantiate (fireEffect, new Vector3 (3.96f, 259.50f, 6.63f), Quaternion.identity);//new Quaternion (339.85f, 259.50f, 6.0623f, 0f)
//	}
}
